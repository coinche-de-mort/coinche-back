package com.cda.epsi23.avl.coincheback.model;

public enum Suit {
    HEART,
    SPADE,
    CLUB,
    DIAMOND
}
