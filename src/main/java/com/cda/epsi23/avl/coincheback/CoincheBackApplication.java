package com.cda.epsi23.avl.coincheback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class CoincheBackApplication {
	public static void main(String[] args) {
		SpringApplication.run(CoincheBackApplication.class, args);
	}

}
