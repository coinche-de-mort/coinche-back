package com.cda.epsi23.avl.coincheback.model;

import java.util.ArrayList;
import java.util.List;

public class Game{
    private List<Player> players;
    private List<GameSet> gameSets;
    private GameSet currentGameSet;
    private final int maxNumberOfPlayers;


    public Game(){
        players = new ArrayList<Player>();
        gameSets = new ArrayList<GameSet>();
        currentGameSet = new GameSet();
        maxNumberOfPlayers = 4;
    }

    public GameSet getCurrentGameSet() {
        return currentGameSet;
    }

    public void setCurrentGameSet(GameSet currentGameSet) {
        this.currentGameSet = currentGameSet;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public int getMaxNumberOfPlayers() {
        return maxNumberOfPlayers;
    }

    public List<GameSet> getGameSets() {
        return gameSets;
    }

    public void setGameSets(List<GameSet> gameSets) {
        this.gameSets = gameSets;
    }
}
