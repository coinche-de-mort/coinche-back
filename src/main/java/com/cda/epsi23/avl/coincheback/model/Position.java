package com.cda.epsi23.avl.coincheback.model;

public enum Position {
    NORTH,
    EAST,
    SOUTH,
    WEST
}
