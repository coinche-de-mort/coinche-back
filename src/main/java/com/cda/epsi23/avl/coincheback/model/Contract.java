package com.cda.epsi23.avl.coincheback.model;

public class Contract {
    private ContractSuit contractSuit;
    private ContractValue contractValue;
    private boolean isCoinche;
    private boolean isSurcoinche;
    private Player player;

    public ContractSuit getContractSuit() {
        return contractSuit;
    }

    public void setContractSuit(ContractSuit contractSuit) {
        this.contractSuit = contractSuit;
    }

    public ContractValue getContractValue() {
        return contractValue;
    }

    public void setContractValue(ContractValue contractValue) {
        this.contractValue = contractValue;
    }

    public boolean isCoinche() {
        return isCoinche;
    }

    public void setCoinche(boolean coinche) {
        isCoinche = coinche;
    }

    public boolean isSurcoinche() {
        return isSurcoinche;
    }

    public void setSurcoinche(boolean surcoinche) {
        isSurcoinche = surcoinche;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
