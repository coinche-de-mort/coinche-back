package com.cda.epsi23.avl.coincheback.model;

public enum Rank {
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING,
    ACE
}
