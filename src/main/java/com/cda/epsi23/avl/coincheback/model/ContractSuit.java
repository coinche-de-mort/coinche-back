package com.cda.epsi23.avl.coincheback.model;

public enum ContractSuit {
    HEART,
    SPADE,
    CLUB,
    DIAMOND,
    NO_TRUMP,
    ALL_TRUMP
}
