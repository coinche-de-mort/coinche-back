package com.cda.epsi23.avl.coincheback.controller;

import com.cda.epsi23.avl.coincheback.model.*;
import com.cda.epsi23.avl.coincheback.service.GameService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

@Controller
public class GameController {

    private GameService gameService;

    @MessageMapping("/news")
    @SendTo("/topic/news")
    public String broadcastNews(@Payload String message) {
        return message;
    }

    @MessageMapping(value = "/game")
    @SendTo("/games-room")
    public Game create(
            String playerName
    ) {
        Player player = new Player();
        Game game = new Game();

        player.setName(playerName);
        player.setPosition(Position.NORTH);

        List<Player> players = new ArrayList<Player>();
        players.add(player);
        game.setPlayers(players);

        this.gameService.create(game);

        return game;
    }
}
