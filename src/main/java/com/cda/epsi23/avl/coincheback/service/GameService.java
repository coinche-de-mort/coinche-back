package com.cda.epsi23.avl.coincheback.service;

import com.cda.epsi23.avl.coincheback.model.*;
import com.cda.epsi23.avl.coincheback.repository.GameRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GameService {
    private GameRepository gameRepository;

    public void create(Game game) {

    }

    /**
     * Ajoute un joueur au jeu.
     * @param game : le jeu en cours
     * @param player : le joueur à ajouter au jeu en cours
     */
    public void addPlayer(Game game, Player player){
        List<Player> players = game.getPlayers();
        players.add(player);
        game.setPlayers(players);
    }

    public void removePlayer(Game game, Player player){
        List<Player> players = game.getPlayers();
        players.remove(player);
        game.setPlayers(players);
    }

    public Game initGame(Game game) {
        List<Player> players = game.getPlayers();
        Game gameUpdated = new Game();
        gameUpdated.setPlayers(players);
        return gameUpdated;
    }

    /*public Game initGameSet(Game game) {
        Game gameUpdated = new Game();

    }*/

    private void distribute(Game game) {
        LinkedList<Card> deck = initDeck();
        Collections.shuffle(deck);
        int iterations = 0;
        List<Player> players = game.getPlayers();

        while (deck.size() > 0) {
            Card card = deck.pop();
            int playerIndex = iterations % game.getMaxNumberOfPlayers();
            Player player = players.get(playerIndex);
            List<Card> hand = player.getHand();
            hand.add(card);
            iterations++;
        }
    }

    private LinkedList<Card> initDeck() {
        LinkedList<Card> deck = new LinkedList<Card>();
        for (Rank rank : Rank.values()) {
            for (Suit suit : Suit.values()) {
                Card card = new Card(rank, suit);
                deck.add(card);
            }
        }
        return deck;
    }
}
