package com.cda.epsi23.avl.coincheback.model;

import java.util.List;

public class GameSet {
    private List<Contract> contracts;
    private Trick currentTrick;
    private List<Trick> tricks;
    private int firstPlayerIndexSet;

    public List<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }

    public Trick getCurrentTrick() {
        return currentTrick;
    }

    public void setCurrentTrick(Trick currentTrick) {
        this.currentTrick = currentTrick;
    }

    public List<Trick> getTricks() {
        return tricks;
    }

    public void setTricks(List<Trick> tricks) {
        this.tricks = tricks;
    }

    public int getFirstPlayerIndexSet() {
        return firstPlayerIndexSet;
    }

    public void setFirstPlayerIndexSet(int firstPlayerIndexSet) {
        this.firstPlayerIndexSet = firstPlayerIndexSet;
    }
}
