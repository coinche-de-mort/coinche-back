package com.cda.epsi23.avl.coincheback;

import com.cda.epsi23.avl.coincheback.model.Game;
import com.cda.epsi23.avl.coincheback.model.Player;
import com.cda.epsi23.avl.coincheback.service.GameService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
public class GameServiceTest {

    private Game game = new Game();
    private Player player = new Player();
    private GameService gameService = new GameService();
    private List<Player> players = new ArrayList<Player>();

    @Test
    public void addPlayerTest() {

        assertThat(game.getPlayers())
                .isEqualTo(players);

        // when
        gameService.addPlayer(game, player);
        players.add(player);

        // then
        assertThat(game.getPlayers())
                .isEqualTo(players);
    }

    @Test
    public void removePlayerTest() {

    }
}
